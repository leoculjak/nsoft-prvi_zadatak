// Regex for catchig blocks of chars inside curly brackets
var regex = /\{([^\{\}])*\}/;

// Recursive function
function trade(sentence, part){
    if(sentence.match(regex) == null){
        return sentence;
    }
    var match = sentence.match(regex);
    var parts;

    match = match[0];
    parts = match.substring(1, match.length -1).split('|');

    sentence = sentence.replace(match, parts[Math.floor(Math.random() * parts.length)]);

    return trade(sentence);
}

var sentence1 = "{Ovo je moj {primjer|zadatak}}";
var sentence2 = "{Danas pada {kiša {i ulice su mokre}| snijeg{ i ulice su {klizave|bijele}}}. Sutra će biti lijepo {vrijeme|šetati}.}";

var changed = trade(sentence2);
console.log(changed);